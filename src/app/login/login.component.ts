import { routes } from './../app.routes';
import { Component, inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { RippleModule } from 'primeng/ripple';
import { PasswordModule } from 'primeng/password';
import { FormsModule } from '@angular/forms';



@Component({
  selector: 'his-login',
  standalone: true,
  imports: [
    CommonModule,
    ButtonModule,
    InputTextModule,
    ToastModule,
    RouterModule,
    RippleModule,
    PasswordModule,
    FormsModule,

  ],

  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [MessageService],
})
export class LoginComponent implements OnInit {
  public messageService = inject(MessageService);
  public value!: string;

  public account:string = '';
  public password:string = '';
  showBottomCenter() {
    this.messageService.add({
      key: 'bc',
      severity: 'success',
      summary: 'Success',
      detail: '登入成功',
    });
  }

  ngOnInit() {
  }
  public submit(){
    if(this.account == 'admin' && this.password == '123'){
      console.log("yes")
    }else{
      console.log("NO")
    }
  }


}
