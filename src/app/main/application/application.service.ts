import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {

  constructor() { }

  #getApplications() {
    return [
      {
        title:'門診醫囑系統',
        icon:'pi pi-bell',
        like:true,
        style:'background-color: green; color:white;',
        routerLink:'/main/news'
      },
      {
        title:'心臟移植表單',
        icon:'pi pi-heart-fill',
        like:true,
        style:'background-color: orange; color:white;'

      },
      {
        title:'電子病歷查詢系統',
        icon:'pi pi-search',
        like:true,
        style:'background-color: #06B6D4;color:white;'

      },
      {
        title:'住院醫囑系統',
        icon:'pi pi-thumbs-up',
        like:false,
        style:'background-color: #EC4899;color:white;'

      },

    ]
  }

  getApplication(){
    return Promise.resolve(this.#getApplications());
  }
}
