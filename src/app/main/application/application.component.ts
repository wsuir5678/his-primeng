import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'his-application',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.scss']
})
export class ApplicationComponent {

}
