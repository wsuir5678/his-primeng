import { SidebarComponent } from '../sidebar/sidebar.component';
import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { DividerModule } from 'primeng/divider';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { TabViewModule } from 'primeng/tabview';
import { NewsService } from './news.service';
import { TagModule } from 'primeng/tag';
import { BadgeModule } from 'primeng/badge';

@Component({
  selector: 'his-news',
  standalone: true,
  imports: [
    CommonModule,
    ButtonModule,
    SidebarComponent,
    ButtonModule,
    DividerModule,
    RouterModule,
    FormsModule,
    DropdownModule,
    TabViewModule,
    TagModule,BadgeModule
  ],
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
  providers: [NewsService],
})
export class NewsComponent {
  news: any;
  newsService = inject(NewsService);
  change = false;

  ngOnInit() {
    this.newsService.getNew().then((result) => {
      this.news = result;
    });
  }
}
