import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor() { }

  #getNews(){
    return [
      {
        date:'2023-07-05 11:11',
        detail:'您目前有22張簽核表需要簽核。',
        chack:'請點選進入',
        tag:true
      },
      {
        date:'2023-07-03 09:32',
        detail:'您目前有22張簽核表需要簽核。',
        chack:'請點選進入'
      },
      {
        date:'2023-06-30 16:29',
        detail:'您目前有22張簽核表需要簽核。',
        chack:'請點選進入'
      },
      {
        date:'2023-06-25',
        detail:'您有衛材申請待審核。',

      },
    ]
  }

  getNew(){
    return Promise.resolve(this.#getNews())
  }
}
