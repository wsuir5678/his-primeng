import { Routes } from '@angular/router';
import { ContainerComponent } from './container/container.component';
import { NewsComponent } from './news/news.component';
import { ApplicationComponent } from './application/application.component';
import { FavoriteComponent } from './favorite/favorite.component';

export const LAYOUT_ROUTES: Routes = [
  { path: '', component: ContainerComponent },
  {
    path: 'news',
    loadComponent: () =>
      import('./news/news.component').then((r) => NewsComponent),
  },
  {
    path: 'application',
    loadComponent: () =>
      import('./application/application.component').then(
        (r) => ApplicationComponent
      ),
  },
  {
    path: 'favorite',
    loadComponent: () =>
      import('./favorite/favorite.component').then(
        (r) => FavoriteComponent
      ),
  },
];
