import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';

@Component({
  selector: 'his-favorite',
  standalone: true,
  imports: [CommonModule,ButtonModule],
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.scss']
})
export class FavoriteComponent implements OnInit {
  public favorite:Array<any>[] = [];

  public ngOnInit(){


  }
}
