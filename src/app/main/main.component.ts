import { RouterModule } from '@angular/router';
import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NewsComponent } from './news/news.component';
import { SplitterModule } from 'primeng/splitter';
import { InputSwitchModule } from 'primeng/inputswitch';
import { AvatarModule } from 'primeng/avatar';
import { FormsModule } from '@angular/forms';
import { FavoriteComponent } from './favorite/favorite.component';
@Component({
  selector: 'his-main',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    SidebarComponent,
    NewsComponent,
    FavoriteComponent,
    SplitterModule,
    AvatarModule,
    InputSwitchModule,
    FormsModule
  ],
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent {
}

