import { Component, OnInit, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { DividerModule } from 'primeng/divider';
import { RouterModule } from '@angular/router';
import { TableModule } from 'primeng/table';
import { NewsService } from '../../news/news.service';

interface News{
  date:string;
  detail:string;
  chack?:string;
}

@Component({
  selector: 'his-news-card',
  standalone: true,
  imports: [
    CommonModule,
    ButtonModule,
    DividerModule,
    TableModule,
    RouterModule,
  ],
  templateUrl: './news-card.component.html',
  styleUrls: ['./news-card.component.scss'],
  providers:[NewsService]
})
export class NewsCardComponent implements OnInit {
  public news:any;
  newsService = inject(NewsService)


  ngOnInit(){
    this.newsService.getNew().then((result) => {
      this.news = result;
    })
  }


}
