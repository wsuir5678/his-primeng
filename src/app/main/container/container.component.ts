import { SidebarComponent } from '../sidebar/sidebar.component';
import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AvatarModule } from 'primeng/avatar';
import { NewsCardComponent } from './news-card/news-card.component';
import { FavouriteCardComponent } from './favourite-card/favourite-card.component';

@Component({
  selector: 'his-container',
  standalone: true,
  imports: [
    CommonModule,
    AvatarModule,
    SidebarComponent,
    NewsCardComponent,
    FavouriteCardComponent
  ],
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss'],
})
export class ContainerComponent {}
