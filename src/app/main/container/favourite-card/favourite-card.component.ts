import { Component, OnInit, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { RouterModule } from '@angular/router';
import { ApplicationService } from '../../application/application.service';

interface Application{
  title?:string;
  icon?:string
  like?:boolean;
  style?:string
  routerLink?:any;
}

@Component({
  selector: 'his-favourite-card',
  standalone: true,
  imports: [CommonModule,
  ButtonModule,RouterModule],
  templateUrl: './favourite-card.component.html',
  styleUrls: ['./favourite-card.component.scss'],
  providers:[ApplicationService]
})
export class FavouriteCardComponent implements OnInit{
  appService = inject(ApplicationService)

  public apps: Application[] = [];

  ngOnInit(): void {
      this.appService.getApplication().then((result) =>{
        this.apps = result;
      })
  }
}
