import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MeauComponent } from './meau/meau.component';

@Component({
  selector: 'his-sidebar',
  standalone: true,
  imports: [CommonModule,MeauComponent],
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {

}
