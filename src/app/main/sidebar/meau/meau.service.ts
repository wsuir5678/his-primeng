import { Injectable } from '@angular/core';
import { PrimeIcons } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class MeauService{

  constructor() { }



  #getMeaus() {
    return [
      {
        // 首頁
        icon: PrimeIcons.HOME,
        routerLink: '/',
      },
      {
        //個人檔案
        icon: PrimeIcons.USER,
        // routerLink: '/',
      },
      {
        // 最新消息
        icon: PrimeIcons.BELL,
        routerLink: '/main/news',
      },
      {
        // 程式集
        icon: PrimeIcons.TH_LARGE,
        // routerLink: '/',
        badge: true,
      },
      {
        // 登出
        icon: PrimeIcons.SIGN_OUT,
        routerLink: '/logout',
      },
    ];
  }

  getMeau(){
    return Promise.resolve(this.#getMeaus());
  }
}
