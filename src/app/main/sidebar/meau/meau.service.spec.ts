import { TestBed } from '@angular/core/testing';

import { MeauService } from './meau.service';

describe('MeauService', () => {
  let service: MeauService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MeauService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
