import { Component, OnInit, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AvatarModule } from 'primeng/avatar';
import { BadgeModule } from 'primeng/badge';
import { ButtonModule } from 'primeng/button';
import { MenubarModule } from 'primeng/menubar';
import { InputSwitchModule } from 'primeng/inputswitch';
import { FormsModule } from '@angular/forms';
import { MeauService } from './meau.service';
import { DialogModule } from 'primeng/dialog';
import { ApplicationService } from '../../application/application.service';
import { DividerModule } from 'primeng/divider';
@Component({
  selector: 'his-meau',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    MenubarModule,
    AvatarModule,
    ButtonModule,
    BadgeModule,
    InputSwitchModule,
    FormsModule,
    DialogModule,
    DividerModule
  ],
  templateUrl: './meau.component.html',
  styleUrls: ['./meau.component.scss'],
  providers: [MeauService],
})
export class MeauComponent implements OnInit {
  meauService = inject(MeauService);
  appService = inject(ApplicationService);
  items?: any;
  checked: boolean = false;
  visible: boolean = false;
  apps: any;
  position: any;

  ngOnInit(): void {
    this.meauService.getMeau().then((results) => {
      this.items = results;
    });
    this.appService.getApplication().then((results) => {
      this.apps = results;
    });
  }

  showDialog() {
    this.position = 'bottom-left';
    this.visible = true;
  }
  getCheck(badge: boolean) {
    if (badge) {
      this.showDialog();
    }
  }
}
