import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MeauComponent } from './meau.component';

describe('MeauComponent', () => {
  let component: MeauComponent;
  let fixture: ComponentFixture<MeauComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MeauComponent]
    });
    fixture = TestBed.createComponent(MeauComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
