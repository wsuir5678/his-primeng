import { Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';

import { MainComponent } from './main/main.component';

export const routes: Routes = [
  {
    path:'',component:LoginComponent
  },
  {
    path:'main',component:MainComponent,
    loadChildren: () => import('./main/main.routes')
    .then(r => r.LAYOUT_ROUTES)
  },
  {
    path:'logout',component:LoginComponent
  }
];
